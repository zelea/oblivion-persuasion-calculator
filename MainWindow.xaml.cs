﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace OblivionPersuasion
{
    public enum Way
    {
        Rotate = 0,
        Admire = 1,
        Joke = 2,
        Coerce = 3,
        Boast = 4,
    }
    public enum Feeling
    {
        Love = 2,
        Like = 1,
        Dislike = -1,
        Hate = -2,
    }
    class Probe
    {
        public Probe(List<Way> path, Feeling[] feelings, float[] levels)
        {
            this.Path = path.Last() == Way.Rotate ? path.SkipLast(1).ToList() : path;
            this.Feelings = feelings;
            this.StartingLevels = levels;
        }
        private float[] StartingLevels { get; }
        private Feeling[] Feelings { get; }
        public List<Way> Path { get; }
        public float Score 
        {
            get
            {
                float score = 0;
                float last;
                List<float> levels = new List<float>(this.StartingLevels);
                foreach (Way item in Path)
                {
                    int index = (int)item - 1;
                    if (item != Way.Rotate)
                    {
                        score += levels.ElementAt(index) * (int)this.Feelings[index];
                    }
                    last = levels.Last();
                    levels.RemoveAt(levels.Count - 1);
                    levels.Insert(0, last);
                }
                return score;
            } 
        }
        public override string ToString() => $"{String.Join("->", Path)}={Score}";
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        public int disposition { get; set; } = 50;
        public bool rotate { get; set; } = false;
        private Feeling[] Feelings 
        {
            get
            {
                var feelings = new Feeling[4];
                for (int i = 0; i < feelings.Length; i++)
                {
                    feelings[i] = FeelingEmojiToEnum[FeelingButtons[i].Content.ToString()];
                }
                return feelings;
            }
            set
            {
                for (int i = 0; i < value.Length; i++)
                {
                    FeelingButtons[i].Content = FeelingEnumToEmoji[value[i]];
                }
            } 
        }
        private float[] Levels 
        {
            get
            {
                float[] levels = new float[4];
                for (int i = 0; i < levels.Length; i++)
                {
                    switch (LevelImages[i].Source.ToString().Split('/').Last())
                    {
                        case @"admire_25.png":
                            levels[i] = 0.25f;
                            break;
                        case @"admire_50.png":
                            levels[i] = 0.5f;
                            break;
                        case @"admire_75.png":
                            levels[i] = 0.75f;
                            break;
                        case @"admire_100.png":
                            levels[i] = 1.0f;
                            break;
                        default:
                            break;
                    }
                }
                return levels;
            }
            set
            {
                Image_Level_Up.Source = new BitmapImage(new Uri(@$"/Resources/admire_{(int)(value[0] * 100)}.png", UriKind.Relative));
                Image_Level_Right.Source = new BitmapImage(new Uri(@$"/Resources/admire_{(int)(value[1] * 100)}.png", UriKind.Relative));
                Image_Level_Down.Source = new BitmapImage(new Uri(@$"/Resources/admire_{(int)(value[2] * 100)}.png", UriKind.Relative));
                Image_Level_Left.Source = new BitmapImage(new Uri(@$"/Resources/admire_{(int)(value[3] * 100)}.png", UriKind.Relative));
            }
        }
        private Button[] FeelingButtons
        {
            get
            {
                return new Button[4]
                {
                    Button_Feeling_Up,
                    Button_Feeling_Right,
                    Button_Feeling_Down,
                    Button_Feeling_Left,
                };
            }
        }
        private Button[] LevelButtons
        {
            get
            {
                return new Button[4]
                {
                    Button_Level_Up,
                    Button_Level_Right,
                    Button_Level_Down,
                    Button_Level_Left,
                };
            }
        }
        private Image[] LevelImages
        {
            get
            {
                return new Image[4]
                {
                    Image_Level_Up,
                    Image_Level_Right,
                    Image_Level_Down,
                    Image_Level_Left,
                };
            }
        }
        public Dictionary<Feeling, string> FeelingEnumToEmoji = new Dictionary<Feeling, string>() { { Feeling.Hate, "😠" }, { Feeling.Dislike, "🙁" }, { Feeling.Like, "🙂" }, { Feeling.Love, "😀" } };
        public Dictionary<string, Feeling> FeelingEmojiToEnum = new Dictionary<string, Feeling>() { { "😠", Feeling.Hate }, { "🙁", Feeling.Dislike }, { "🙂", Feeling.Like }, { "😀", Feeling.Love } };
        private Feeling[] feelings = Enum.GetValues<Feeling>();
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;
            Feelings = feelings;
            Levels = new float[4] { 0.25f, 0.5f, 0.75f, 1.0f };
        }
        private void Button_Feeling_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            int bindex = Array.IndexOf(FeelingButtons, button);
            int findex = Array.IndexOf(feelings, Feelings[bindex]);
            FeelingButtons[bindex].Content = FeelingEnumToEmoji[feelings[(findex + 1) % feelings.Length]];
        }
        private void Button_Calculate_Click(object sender, RoutedEventArgs e)
        {
            ListBox_Results.Items.Clear();
            List<Probe> probes = new List<Probe>();

            IEnumerable<IEnumerable<Way>> paths;
            if (rotate)
            {
                paths = GetPermutations(Enum.GetValues<Way>());
            }
            else
            {
                paths = GetPermutations(Enum.GetValues<Way>().Skip(1));
            }

            foreach (var path in paths)
            {
                probes.Add(new Probe(path.ToList(), Feelings, Levels));
            }

            probes = probes.OrderByDescending(p => p.Score).ToList();

            foreach (Probe probe in probes)
            {
                ListBox_Results.Items.Add(probe.ToString());
            }
        }

        static IEnumerable<IEnumerable<T>> GetPermutations<T>(IEnumerable<T> list, int length)
        {
            if (length == 1) return list.Select(t => new T[] { t });

            return GetPermutations(list, length - 1)
                .SelectMany(t => list.Where(e => !t.Contains(e)),
                    (t1, t2) => t1.Concat(new T[] { t2 }));
        }
        static IEnumerable<IEnumerable<T>> GetPermutations<T>(IEnumerable<T> list)
        {
            if (list.Count() == 1) return list.Select(t => new T[] { t });

            return GetPermutations(list, list.Count() - 1)
                .SelectMany(t => list.Where(e => !t.Contains(e)),
                    (t1, t2) => t1.Concat(new T[] { t2 }));
        }
        private void Button_Level_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            int index = Array.IndexOf(LevelButtons, button);
            Image image = LevelImages[index];
            float level = Levels[index];
            float nextLevel = (level + 0.25f) % 1;
            if (nextLevel == 0)
                nextLevel = 1f;
            image.Source = new BitmapImage(new Uri(@$"/Resources/admire_{nextLevel * 100}.png", UriKind.Relative));
        }

        private void ConflictCheck(object sender, EventArgs e)
        {
            bool enabled = Feelings.Distinct().Count() == 4 && Levels.Distinct().Count() == 4;
            Button_Calculate.IsEnabled = enabled;
        }
    }
}
